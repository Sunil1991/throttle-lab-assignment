Steps to run this project on local and production

Local setup
 
1) git clone git clone https://Sunil1991@bitbucket.org/Sunil1991/throttle-lab-assignment.git
2) create virtualenv and activate it.
3) pip install requirements.txt in virtualenv
4) Run python manage.py makemigrations followed by python manage.py migrate 
5)There are two django custom commands
    1) Run python manage.py UploadJsonToDb usermgmt.management/commands/TestJson.txt, this command will dump formatted json data in respective database
    2) Run python manage.py PopulateDBdata.py, to check dummy json data on console. 
6) Run python manage.py runserver
7) Then hit url http://127.0.0.1:8000/api/v1/users



Production setup on Pythonanywhere

1) git clone git clone https://Sunil1991@bitbucket.org/Sunil1991/throttle-lab-assignment.git
2) create virtualenv and activate it.
3) pip install requirements.txt in virtualenv
4) Run python manage.py makemigrations followed by python manage.py migrate 
5)There are two django custom commands
    1) Run python manage.py UploadJsonToDb usermgmt.management/commands/TestJson.txt, this command will dump formatted json data in respective database
    2) Run python manage.py PopulateDBdata.py, to check dummy json data on console. 

6)Setup pythonanywhere account and set up wsgi.py file with this piece of code and replace directory path according to manage.py locaction and settings.py location 
        """ import os
        import sys
        path = '/home/ramuklinus/throttle-lab-assignment/throttlelab'
        if path not in sys.path:
            sys.path.append(path)
        
        os.environ['DJANGO_SETTINGS_MODULE'] = 'throttlelab.settings'
        
        # then:
        from django.core.wsgi import get_wsgi_application
        application = get_wsgi_application()
        """
7) Then hit url 'http://ramuklinus.pythonanywhere.com/api/v1/user/'

