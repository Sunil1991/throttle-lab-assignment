from django.db import models
from django.contrib.auth.models import AbstractBaseUser,UserManager,PermissionsMixin
from django.utils import timezone
from django.conf import settings
import uuid

class User(AbstractBaseUser,PermissionsMixin):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(verbose_name="email",max_length=60,unique=True)
    username = models.CharField(max_length=30,unique=False)
    date_joined = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)
    first_name = models.CharField(max_length=254, blank=True)
    last_name = models.CharField(max_length=254, blank=True)
    time_zone = models.CharField(max_length=30,blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    def __str__(self):
        return   self.username +' - '+ self.email

    def has_perm(self,perm,obj=None):
        return self.is_admin

    def has_module_perms(self,app_label):
        return True

    def get_short_name(self):
        if self.first_name:
            return self.first_name.strip()
        elif self.username:
            return self.username.strip()
        elif self.email:
            return self.email.strip()
        else:
            return 'unknown'

    @property
    def activity_periods(self):
        return [{'start_time':s.start_time.strftime('%b %d %Y %I:%M%p'),'end_time':s.end_time.strftime('%b %d %Y %I:%M%p')}  for s in UserLoginActivity.objects.filter(user_id=self.id.hex)]

class UserLoginActivity(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    start_time =  models.DateTimeField(null=True, blank=True)
    end_time =  models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return  self.user.username +' - '+str(self.start_time) +' - '+ str(self.end_time)
