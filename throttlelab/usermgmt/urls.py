from rest_framework import routers
from django.conf.urls import url,include

from .views import UserViewSet

router = routers.DefaultRouter()
router.register(r'user', UserViewSet)
slashless_router = routers.DefaultRouter(trailing_slash=False)
slashless_router.registry = router.registry[:]
urlpatterns = [
    url(r'^', include(router.urls)), url(r'^', include(slashless_router.urls)),
]

