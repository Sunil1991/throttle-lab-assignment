"""
Script to add json data into database
"""
import json
import argparse
from django.core.management.base import BaseCommand
from datetime import datetime
import os

from django.db.transaction import atomic
from .. .models import User,UserLoginActivity


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")


class Command(BaseCommand):
    help = "Import data from json file to user and user activity table"

    def add_arguments(self, parser):
        parser.add_argument('txt', nargs='?', type=argparse.FileType('r'))
    @atomic
    def handle(self, *args, **options):
        with options['txt'] as json_file:
            data = json.load(json_file)
            user_activity = (data['members'])
            u = ''
            a = ''
            try:
                for activity in user_activity:
                    user = User()
                    username = activity['real_name'].strip().replace(' ', '').lower()
                    user.email = username+'@gmail.com'
                    user.username = username
                    user.set_password(username)
                    user.is_staff = True
                    user.is_admin = True
                    user.time_zone = activity['tz']
                    user.save()
                    u = user
                    login_activities = activity['activity_periods']
                    for login_activity in login_activities:
                        user_login_activity = UserLoginActivity()
                        user_login_activity.user = user
                        user_login_activity.end_time = datetime.strptime(login_activity['end_time'],'%b %d %Y %I:%M%p')
                        user_login_activity.start_time = datetime.strptime(login_activity['start_time'], '%b %d %Y %I:%M%p')
                        user_login_activity.save()
                        a = user_login_activity
            except Exception as e:
                print("Error is - ",e,u,a)