import json
import argparse
from django.core.management.base import BaseCommand
from datetime import datetime
import os

from django.db.transaction import atomic
from rest_framework.renderers import JSONRenderer

from .. .models import User,UserLoginActivity
from .. .serializers import UserSerializer,UserLoginActivitySerializer


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")


class Command(BaseCommand):
    help = "Import data from database and show in json form"

    def add_arguments(self, parser):
        parser.add_argument('txt', nargs='?', type=argparse.FileType('r'))
    def handle(self, *args, **options):
        user_queryset = User.objects.filter().all()
        serializer = UserSerializer(user_queryset, many=True)
        json = JSONRenderer().render(serializer.data)
        print(json)

