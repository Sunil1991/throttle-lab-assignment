from django.contrib import admin

# Register your models here.
from .models import User,UserLoginActivity


class UserAdmin(admin.ModelAdmin):
    search_fields = ('first_name','email','username')

admin.site.register(User,UserAdmin)


class UserLoginActivityAdmin(admin.ModelAdmin):
    search_fields = ('id',)

admin.site.register(UserLoginActivity,UserLoginActivityAdmin)
