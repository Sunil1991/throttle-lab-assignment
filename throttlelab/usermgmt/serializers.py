from .models import User,UserLoginActivity
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'username', 'time_zone','activity_periods')
        model = User
class UserLoginActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserLoginActivity
