from django.http import HttpResponse
# Create your views here.
from rest_framework import permissions, viewsets
from rest_framework.authentication import BasicAuthentication
from rest_framework.renderers import JSONRenderer

from .models import User,UserLoginActivity
from .serializers import UserSerializer,UserLoginActivitySerializer

class UserViewSet(viewsets.ModelViewSet):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_queryset(self):
        """
        Override get_queryset method to return filtered objects
        """
        queryset = User.objects.filter(id=self.request.user.id)
        return queryset

    """
        API endpoint that allows users to get user login activity.
    
    """
    def list(self, request, *args, **kwargs):
        user_queryset = User.objects.filter().all()
        serializer = UserSerializer(user_queryset, many=True,context={'request': request})
        json = JSONRenderer().render(serializer.data)
        return HttpResponse(json, content_type="application/json")
